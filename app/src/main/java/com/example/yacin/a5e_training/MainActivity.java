package com.example.yacin.a5e_training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void add_student_btn_clicked(View view) {
        Intent intent = new Intent(this, AddStudent.class);
        startActivity(intent);
    }

    public void effect_presence_btn_clicked(View view) {
        Intent intent_2 = new Intent(this, StudentSearch.class);
        startActivity(intent_2);
    }

    public void payement_btn_clicked(View view) {
        Intent intent_3 = new Intent(this, EffectuerPayement.class);
        startActivity(intent_3);

    }
}

package com.example.yacin.a5e_training;


public class Etudiant {

    private String _nom;
    private String _prenom;
    private String _telephone;
    private String _formation;
    private String _groupe;
    private int _nbr_scence_etudier;
    private int _nbr_scence_paye;

    public Etudiant(){

    }

    public Etudiant(String _nom, String _prenom, String _telephone,
                    String _formation, String _groupe,
                    int _nbr_scence_etudier, int _nbr_scence_paye) {
        this._nom = _nom;
        this._prenom = _prenom;
        this._telephone = _telephone;
        this._formation = _formation;
        this._groupe = _groupe;
        this._nbr_scence_etudier = _nbr_scence_etudier;
        this._nbr_scence_paye = _nbr_scence_paye;
    }

    public String get_nom() {
        return _nom;
    }

    public void set_nom(String _nom) {
        this._nom = _nom;
    }

    public String get_prenom() {
        return _prenom;
    }

    public void set_prenom(String _prenom) {
        this._prenom = _prenom;
    }

    public String get_telephone() {
        return _telephone;
    }

    public void set_telephone(String _telephone) {
        this._telephone = _telephone;
    }

    public String get_formation() {
        return _formation;
    }

    public void set_formation(String _formation) {
        this._formation = _formation;
    }

    public String get_groupe() {
        return _groupe;
    }

    public void set_groupe(String _groupe) {
        this._groupe = _groupe;
    }

    public int get_nbr_scence_etudier() {
        return _nbr_scence_etudier;
    }

    public void set_nbr_scence_etudier(int _nbr_scence_etudier) {
        this._nbr_scence_etudier = _nbr_scence_etudier;
    }

    public int get_nbr_scence_paye() {
        return _nbr_scence_paye;
    }

    public void set_nbr_scence_paye(int _nbr_scence_paye) {
        this._nbr_scence_paye = _nbr_scence_paye;
    }
}

package com.example.yacin.a5e_training;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHandler extends SQLiteOpenHelper {

    private static  final int DATABASE_VERSION = 1;
    private static  final String DATABASE_NAME = "Etudiants.db";
    private static  final String TABLE_ETUDIANTS = "Etudians";
    private static  final String COLUMN_NOM = "_nom";
    private static  final String COLUMN_PRENOM = "_prenom";
    private static  final String COLUMN_TELEPHONE = "_telephone";
    private static  final String COLUMN_FORMATION = "_formation";
    private static  final String COLUMN_GROUPE = "_groupe";
    private static  final String COLUMN_NBR_SCENCE_ETUDIER = "_nbr_scence_etudier";
    private static  final String COLUMN_NBR_SCENCE_PAYE = "_nbr_scence_paye";

    public  DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_ETUDIANTS + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT , " +
                COLUMN_NOM + " TEXT , " +
                COLUMN_PRENOM + " TEXT , " +
                COLUMN_TELEPHONE + " TEXT , " +
                COLUMN_FORMATION + " TEXT , " +
                COLUMN_GROUPE + " TEXT , " +
                COLUMN_NBR_SCENCE_ETUDIER + " INTEGER , " +
                COLUMN_NBR_SCENCE_PAYE + " INTEGER );";
        db.execSQL(query);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ETUDIANTS + " ;");
        onCreate(db);
    }

//    add new student
    public void add_etudiant(Etudiant etudiant){
        ContentValues values = new ContentValues();

        values.put(COLUMN_NOM, etudiant.get_nom());
        values.put(COLUMN_PRENOM, etudiant.get_prenom());
        values.put(COLUMN_TELEPHONE, etudiant.get_telephone());
        values.put(COLUMN_FORMATION, etudiant.get_formation());
        values.put(COLUMN_GROUPE, etudiant.get_groupe());
        values.put(COLUMN_NBR_SCENCE_ETUDIER, etudiant.get_nbr_scence_etudier());
        values.put(COLUMN_NBR_SCENCE_PAYE, etudiant.get_nbr_scence_paye());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_ETUDIANTS, null, values);
        db.close();

    }

    public Etudiant[] db_to_array(){

        String dbstring = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_ETUDIANTS + " WHERE 1 ;";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        c.getCount();


        Etudiant[] etudiant_array = new Etudiant[c.getCount()];
        int i = 0;

//        while (! c.isAfterLast()){
//            if (c.getString(c.getColumnIndex("_nom")) != null){
//                dbstring += c.getString(c.getColumnIndex("_nom"));
//                dbstring += c.getString(c.getColumnIndex("_prenom"));
//                dbstring += c.getString(c.getColumnIndex("_telephone"));
//                dbstring += c.getString(c.getColumnIndex("_groupe"));
//                dbstring += c.getString(c.getColumnIndex("_formation"));
//                dbstring += c.getString(c.getColumnIndex("_nbr_scence_etudier"));
//                dbstring += c.getString(c.getColumnIndex("_nbr_scence_paye"));
//            }
//            c.moveToNext();
//        }

        while (! c.isAfterLast()){
            if (c.getString(c.getColumnIndex("_nom")) != null){
                Etudiant etudiant = new Etudiant();
                etudiant.set_nom(c.getString(c.getColumnIndex("_nom")));
                etudiant.set_prenom(c.getString(c.getColumnIndex("_prenom")));
                etudiant.set_telephone(c.getString(c.getColumnIndex("_telephone")));
                etudiant.set_formation(c.getString(c.getColumnIndex("_formation")));
                etudiant.set_groupe(c.getString(c.getColumnIndex("_groupe")));
                etudiant.set_nbr_scence_etudier(Integer.parseInt(c.getString(c.getColumnIndex("_nbr_scence_etudier"))));
                etudiant.set_nbr_scence_paye(Integer.parseInt(c.getString(c.getColumnIndex("_nbr_scence_paye"))));

                etudiant_array[i] = etudiant;
                i++;

            }
            c.moveToNext();
        }


        db.close();
        return etudiant_array;

    }


    public Etudiant[] searched_students_to_array(String formation, String groupe){

        String dbstring = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_ETUDIANTS + " WHERE " + COLUMN_FORMATION +
                " = \"" + formation + "\" AND " + COLUMN_GROUPE + " = \"" + groupe + "\" ;";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        c.getCount();


        Etudiant[] etudiant_array = new Etudiant[c.getCount()];
        int i = 0;

        while (! c.isAfterLast()){
            if (c.getString(c.getColumnIndex("_nom")) != null){
                Etudiant etudiant = new Etudiant();
                etudiant.set_nom(c.getString(c.getColumnIndex("_nom")));
                etudiant.set_prenom(c.getString(c.getColumnIndex("_prenom")));
                etudiant.set_telephone(c.getString(c.getColumnIndex("_telephone")));
                etudiant.set_formation(c.getString(c.getColumnIndex("_formation")));
                etudiant.set_groupe(c.getString(c.getColumnIndex("_groupe")));
                etudiant.set_nbr_scence_etudier(Integer.parseInt(c.getString(c.getColumnIndex("_nbr_scence_etudier"))));
                etudiant.set_nbr_scence_paye(Integer.parseInt(c.getString(c.getColumnIndex("_nbr_scence_paye"))));

                etudiant_array[i] = etudiant;
                i++;

            }
            c.moveToNext();
        }


        db.close();
        return etudiant_array;
    }

    public void effectuer_presence(String telephone){

        int current_nbr_science_etudier = 2;

        String query = "SELECT * FROM " + TABLE_ETUDIANTS + " WHERE " + COLUMN_TELEPHONE + " = \"" +
                telephone + "\" ;";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();

        current_nbr_science_etudier = Integer.parseInt(c.getString(c.getColumnIndex(COLUMN_NBR_SCENCE_ETUDIER)));

        Log.i("tag_test", String.valueOf(current_nbr_science_etudier));

//        ContentValues values = new ContentValues();
//        values.put(COLUMN_NBR_SCENCE_ETUDIER,String.valueOf(current_nbr_science_etudier+1));

//        db.update(TABLE_ETUDIANTS,values,COLUMN_TELEPHONE + " = \"" +telephone+"\"",null);
//        db.close();

        String query_2 = "UPDATE " + TABLE_ETUDIANTS + " SET " + COLUMN_NBR_SCENCE_ETUDIER +
                " = \"" + String.valueOf(current_nbr_science_etudier+1) + "\" WHERE " +
                COLUMN_TELEPHONE + " = \"" + telephone + "\" ;";

        db.execSQL(query_2);
        db.close();

    }

    public Etudiant search_etudiant_for_payement(String telephone){

        String query = "SELECT * FROM " + TABLE_ETUDIANTS + " WHERE " + COLUMN_TELEPHONE + " = \"" +
                telephone + "\" ;";

//        String query_2 = "SELECT * FROM " + TABLE_ETUDIANTS + " WHERE " + COLUMN_TELEPHONE + " = \"0000\";";

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();

//        Etudiant etudiant_1 = new Etudiant("a","b","1","d","2",7,5) ;
        Etudiant etudiant_1 = new Etudiant();

//        while (! c.isAfterLast()){
//            if (c.getString(c.getColumnIndex("_nom")) != null){
//
//            }
//            c.moveToNext();
//        }
        Log.i("test_tag", c.getString(c.getColumnIndex("_nom")));
        etudiant_1.set_nom(c.getString(c.getColumnIndex("_nom")));
        etudiant_1.set_prenom(c.getString(c.getColumnIndex("_prenom")));
        etudiant_1.set_telephone(c.getString(c.getColumnIndex("_telephone")));
        etudiant_1.set_formation(c.getString(c.getColumnIndex("_formation")));
        etudiant_1.set_groupe(c.getString(c.getColumnIndex("_groupe")));
        etudiant_1.set_nbr_scence_etudier(Integer.parseInt(c.getString(c.getColumnIndex("_nbr_scence_etudier"))));
        etudiant_1.set_nbr_scence_paye(Integer.parseInt(c.getString(c.getColumnIndex("_nbr_scence_paye"))));

        db.close();

        return etudiant_1;
    }

    public void effectuer_payement(String telephone){
        int current_nbr_science_paye = 2;

        String query = "SELECT * FROM " + TABLE_ETUDIANTS + " WHERE " + COLUMN_TELEPHONE + " = \"" +
                telephone + "\" ;";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();

        current_nbr_science_paye = Integer.parseInt(c.getString(c.getColumnIndex(COLUMN_NBR_SCENCE_PAYE)));

        Log.i("tag_test", String.valueOf(current_nbr_science_paye));

//        ContentValues values = new ContentValues();
//        values.put(COLUMN_NBR_SCENCE_ETUDIER,String.valueOf(current_nbr_science_etudier+1));

//        db.update(TABLE_ETUDIANTS,values,COLUMN_TELEPHONE + " = \"" +telephone+"\"",null);
//        db.close();

        String query_2 = "UPDATE " + TABLE_ETUDIANTS + " SET " + COLUMN_NBR_SCENCE_PAYE +
                " = \"" + String.valueOf(current_nbr_science_paye+1) + "\" WHERE " +
                COLUMN_TELEPHONE + " = \"" + telephone + "\" ;";

        db.execSQL(query_2);
        db.close();

    }

}

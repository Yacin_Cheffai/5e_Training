package com.example.yacin.a5e_training;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class EffectuerPresence extends AppCompatActivity implements AlertFragment.NoticeDialogListener{

    String formation;
    String groupe;
    String telephone;
    public static String head_dialog;

    DBHandler dbhandler = new DBHandler(this,null,null,1);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_effectuer_presence);

        Intent intent = getIntent();
        formation = intent.getStringExtra(StudentSearch.key_intent_formation);
        groupe = intent.getStringExtra(StudentSearch.key_intent_groupe);

        Etudiant[] searched_students = dbhandler.searched_students_to_array(formation, groupe);

        final ArrayList<String> students = new ArrayList<>();
        final ArrayList<String> telephones = new ArrayList<>();

        for (Etudiant etudiant:searched_students) {

            String full_student_name;
            full_student_name = etudiant.get_nom() + " " + etudiant.get_prenom() + " \n etudier : "
                    + etudiant.get_nbr_scence_etudier() + " \n payé : " +
                    etudiant.get_nbr_scence_paye();
            students.add(full_student_name);
            telephones.add(etudiant.get_telephone());
        }

        ListAdapter students_list_adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, students);
        ListView students_list_view = (ListView) findViewById(R.id.student_list_view);
        students_list_view.setAdapter(students_list_adapter);

        students_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(EffectuerPresence.this, telephones.get(position), Toast.LENGTH_LONG).show();
                telephone = telephones.get(position);
                head_dialog = students.get(position);
                showAlertDialog();
            }
        });

        for (String etd: students) {
            Log.i("test",etd);
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        dbhandler.effectuer_presence(telephone);
        Intent intent_to_releod = getIntent();
        finish();
        startActivity(intent_to_releod);
        Toast.makeText(this, "presence a été effectué", Toast.LENGTH_LONG).show();


    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    public void showAlertDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new AlertFragment();
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }
}
